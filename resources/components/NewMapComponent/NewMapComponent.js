/**
 *
 * NewMapComponent
 *
 * <DESCRIPTION>
 *

Changelog 2013-11-04 -------------
Support for representing data as a colored shape in a map,
e.g. population of a country using a color map.

New CDE properties:
- mapMode = markers, shapes, just the map
- colormap = [[0,0,0,255],[255,255,255,0]] (RGBA array for defining the colormap of the shapes)
- shapeSource: file/url with the shape definitions
- shapeMouseOver, shapeMouseOut, shapeMouseClick: callbacks for handling shapes
- tilesets: which of the tilesets to use for rendering the map (added support for 30 tilesets)

Internal changes:
- renamed OpenStreetMapEngine to OpenLayersEngine
- added a bunch of functions to the map engines

Features:

1) SHAPES:
Loading of shapes in the following file formats
- JSON (not quite the same format as Kleyson's component)
- KML (google earth)

Goodies:
- data can be exported to JSON format
- possibility to reduce the number of points (useful for importing KML and exporting to JSON)
- abstracted the interface to the map engines for simple tasks like changing the color of the shape on mouseover/click

TODO:
- smart detection of columns in the datasource, as it is currently done with the markers. Currently key=column[0], value = column[1]
- ability to have both markers and shapes in the same datasource
- ability to load the shape definitions *in paralell* with the datasource

2) TILES (png images representing the map)
OpenStreetMaps is ugly, I found many nicer tilesets that work in both map engines (google/openlayers)
To compare the various tilesets, visit http://mc.bbbike.org/mc/?num=2

 Example of valid values for the CDE property "tilesets"
   'mapquest'
   ['mapquest']
   ['mapquest', 'apple']
   'custom/static/localMapService/${z}/${x}/${y}.png'
   "http://otile1.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png"
   "http://otile{switch:1,2,3,4}.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png"

3) TODOs/Ideas
- Use OpenLayers.Control.GetFeature for getting click/hover callbacks
- Use GeoJSON for internal representation of shapes
- Write map engine for jvectormap


 *
 */


(function() {

    var geonames = {
        name: "geonames",
        label: "GeoNames",
        defaults: {
        },
        implementation: function (tgt, st, opt) {
            var location;
            var name = st.address;
            var featureClass;
            if (!name) {
        	//Check city
        	if (st.city) {
        	    name = st.city;
        	    featureClass = 'P';
        	} else if (st.county) {
        	    name = st.county;
        	    featureClass = 'A';
        	} else if (st.region) {
        	    name = st.region;
        	    featureClass = 'A';
        	} else if (st.state) {
        	    name=st.state;
        	    featureClass = 'A';
        	} else if (st.country) {
        	    name = st.country;
        	    featureClass = 'A';
        	}
            }



            var opts =            {
                q: name,
                maxRows: 1,
                dataType: "json",
                featureClass: featureClass
            };



	    var callBackName = 'GeoNameContinuation' + $.now() + st.position;
	    window[callBackName] = function (result) {
		if (result.geonames && result.geonames.length > 0) {
    		    location = [parseFloat(result.geonames[0].lng),
        			parseFloat(result.geonames[0].lat)];
		    st.continuationFunction(location);
		}
	    };


	    name = name.replace(/&/g,",");
	    var request = 'http://ws.geonames.org/searchJSON?q=' +  encodeURIComponent(name)  + '&maxRows=1&callback=' + callBackName;
            request += ( featureClass ? '&featureClass=' + featureClass : '' ) ;


	    var aObj = new JSONscriptRequest(request);
	    // Build the script tag
	    aObj.buildScriptTag();
	    // Execute (add) the script tag
	    aObj.addScriptTag();
        }
    };
    Dashboards.registerAddIn("NewMapComponent", "LocationResolver", new AddIn(geonames));


    var urlMarker = {
  	name: "urlMarker",
  	label: "Url Marker",
  	defaults: {
  	    defaultUrl:  'getResource/system/pentaho-cdf-dd/resources/custom/components/NewMapComponent/images/marker_grey.png'
  	},
  	implementation: function (tgt, st, opt) {
  	    if (st.url)
	  	return st.url;

	    if (st.position) {
	  	switch (st.position % 5) {
	  	case 0:
	  	    return 'getResource/system/pentaho-cdf-dd/resources/custom/components/NewMapComponent/images/marker_grey.png';
	  	case 1:
	  	    return 'getResource/system/pentaho-cdf-dd/resources/custom/components/NewMapComponent/images/marker_blue.png';
	  	case 2:
	  	    return 'getResource/system/pentaho-cdf-dd/resources/custom/components/NewMapComponent/images/marker_grey02.png';
	  	case 3:
	  	    return 'getResource/system/pentaho-cdf-dd/resources/custom/components/NewMapComponent/images/marker_orange.png';
	  	case 4:
	  	    return 'getResource/system/pentaho-cdf-dd/resources/custom/components/NewMapComponent/images/marker_purple.png';
	  	}
	    }

	    return opt.defaultUrl;
  	}
    };
    Dashboards.registerAddIn("NewMapComponent", "MarkerImage", new AddIn(urlMarker));

    var cggMarker = {
  	name: "cggMarker",
  	label: "CGG Marker",
  	defaults: {
  	},
  	implementation: function (tgt, st, opt) {
  	    var url = '../cgg/draw?script=' + st.cggGraphName;

  	    var width = st.width;
  	    var height = st.height;
  	    var cggParameters = {};
  	    if (st.width) cggParameters.width = st.width;
  	    if (st.height) cggParameters.height = st.height;

  	    for (parameter in st.parameters) {
  		cggParameters[parameter] = st.parameters[parameter];
  	    }

  	    for(parameter in cggParameters){
    	        if( cggParameters[parameter] !== undefined ){
        	    url += "&param" + parameter + "=" + encodeURIComponent( cggParameters[parameter] ) ;
	        }
    	    }

	    return url;

  	}
    };
    Dashboards.registerAddIn("NewMapComponent", "MarkerImage", new AddIn(cggMarker));

})();



var ShapeConversionMixin = {
    // Mixin for loading map shape data from a KML
    // Assume always that latitude and longitude are angular coordinates in decimal format (Google's default)

    parseShapeKey : undefined, // parseKey(placemark): function used to generate the key associated with the shape
    getShapeFromKML: function (rawData) {
        /*
         Parse a KML file, return a JSON dictionary where each key is associated with an array of shapes of the form
         mymap = {'Cascais:'[ [[lat0, long0],[lat1, long1]] ]}; // 1 array with a list of points
         */
        var myself = this,
            mymap = {};

        var result = $(rawData).find('Placemark').each( function(idx, y){
            var key;
            if (typeof myself.parseShapeKey == "function"){
                try {
                    key = myself.parseShapeKey(y);
                } catch (e) {
                    key = $(y).find('name').text();
                }
            } else {
                key = $(y).find('name').text();
            }

            // Create an array for the strings that define the (closed) curves in a Placemark
            var polygonArray = _.map($(y).find('Polygon'), function (yy){
                var polygon = [];
                _.each(['outerBoundaryIs', 'innerBoundaryIs'], function (b) {
                    var polygonObj = $(yy).find(b + ' LinearRing coordinates');
                    //if (polygonObj.length >0){
                        _.each(polygonObj, function (v) {
                            var s = $(v).text().trim();
                            if (s.length > 0){
                                var p = _.map(s.split(' '), function(el){
                                    return _.map(el.split(',').slice(0,2), parseFloat).reverse();
                                });
                                //p =  this.reducePoints(p.slice(0, pp.length -1), precision_m); // this would reduce the number of points in the shape
                                polygon.push( p );
                            }
                        });
                    //}
                });
                return polygon;
            });


            if (_.isEmpty(polygonArray)){
  	        return;
            }
            // var pp = _.map(polygonArray, function (polygon) {
            //     _.each(polygon, function (ppolygon) {
            //     var p = _.map(ppolygon.split(' '), function(el){
            //         return _.map(el.split(',').slice(0,2), parseFloat).reverse();
            //     });
            //        return p;
            //         });

                //var p =  this.reducePoints(pp.slice(0, pp.length -1), precision_m); // this would reduce the number of points in the shape
                //var p =pp;
                if (!mymap[key]) {
                    mymap[key] = polygonArray;
                }// else {
                //    mymap[key].push([p.slice(0)]);
                //}
            //});
        });

        return mymap;
    },

    reducePoints: function (points, precision_m){
        if (precision_m < 0){
            return points;
        }
        function properRDP(points,epsilon){
            /*
             *** Ramer Douglas Peucker, from http://karthaus.nl/rdp/js/rdp.js

             The Ramer-Douglas–Peucker algorithm is an algorithm for reducing the number of points in a curve that is approximated by a series of points.
             It does so by "thinking" of a line between the first and last point in a set of points that form the curve.
             It checks which point in between is farthest away from this line.
             If the point (and as follows, all other in-between points) is closer than a given distance 'epsilon', it removes all these in-between points.
             If on the other hand this 'outlier point' is farther away from our imaginary line than epsilon, the curve is split in two parts.
             The function is recursively called on both resulting curves, and the two reduced forms of the curve are put back together.

             1) From the first point up to and including the outlier
             2) The outlier and the remaining points.


             *** Bad implementations on the web
             On the web I found many Ramer Douglas Peucker implementations, but most of the top results on google contained bugs.
             Even the original example on Wikipedia was BAD!
             The bugs were ranging from bad calculation of the perpendicular distance of a point to a line (often they contained a devide by zero error for vertical lines),
             to discarding points that should not be removed at all.
             To see this in action, just try running the algorithm on it's own result with the same epsilon,
             many implementations will keep on reducing more and more points until there is no spline left.
             A correct implementation of RDP will remove *all* points that it can remove given a certain epsilon in the first run.

             I hope that by looking at this source code for my Ramer Douglas Peucker implementation you will be able to get a correct reduction of your dataset.

             @licence Feel free to use it as you please, a mention of my name is always nice.

             Marius Karthaus
             http://www.LowVoice.nl

             *
             */
            var firstPoint=points[0];
            var lastPoint=points[points.length-1];
            if (points.length<3){
                return points;
            }
            var index=-1;
            var dist=0;
            for (var i=1;i<points.length-1;i++){
                var cDist=findPerpendicularDistance(points[i],firstPoint,lastPoint);
                if (cDist>dist){
                    dist=cDist;
                    index=i;
                }
            }
            if (dist>epsilon){
                // iterate
                var l1=points.slice(0, index+1);
                var l2=points.slice(index);
                var r1=properRDP(l1,epsilon);
                var r2=properRDP(l2,epsilon);
                // concat r2 to r1 minus the end/startpoint that will be the same
                var rs=r1.slice(0,r1.length-1).concat(r2);
                return rs;
            }else{
                return [firstPoint,lastPoint];
            }
        }
        function findPerpendicularDistance(p, p1,p2) {
            // if start and end point are on the same x the distance is the difference in X.
            var result;
            var slope;
            var intercept;
            if (p1[0]==p2[0]){
                result=Math.abs(p[0]-p1[0]);
            }else{
                slope = (p2[1] - p1[1]) / (p2[0] - p1[0]);
                intercept = p1[1] - (slope * p1[0]);
                result = Math.abs(slope * p[0] - p[1] + intercept) / Math.sqrt(Math.pow(slope, 2) + 1);
            }

            return result;
        }
        return properRDP(points, precision_m/6.3e6);

    }, // reducePoints

    exportShapeDefinition: function () {
        if (this.shapeDefinition){
            window.open( "data:text/json;charset=utf-8," + escape(JSON.stringify(this.shapeDefinition)));
        }
    }

};


var ColorMapMixin = {
    /** Mixin for handling color maps
     This should probably be elevated to a proper class with a nice database of colormaps
     */
    colormap: [[0, 102, 0, 255], [255, 255 ,0,255], [255, 0,0, 255]], //RGBA
    colormaps: {
        'jet': [],
        'gray': [[0,0,0,255],[255,255,255,255]],
        'french-flag': [[255,0,0,255],[255,254,255,255], [0,0,255,255]]
    },
    getColorMap: function() {

        var interpolate = function(a, b, n){
            var c = [], d=[];
            var k, kk, step;
            for (k=0; k<a.length; k++){
                c[k] = [];
                for (kk=0, step = (b[k]-a[k])/n; kk<n; kk++){
                    c[k][kk] = a[k] + kk*step;
                }
            }
            for (k=0; k<c[0].length; k++){
                d[k] = [];
                for (kk=0; kk<c.length; kk++){
                    d[k][kk] =  Math.round(c[kk][k]);
                }
            }
            return d;
        };
        var cmap = [];
        for (k=1; k<this.colormap.length; k++)
        {
            cmap = cmap.concat(interpolate(this.colormap[k-1], this.colormap[k], 32));
        }
        return _.map( cmap, function (v) {
            return 'rgba('+ v.join(',') +')';
        });
    },
    mapColor: function (value, minValue, maxValue, colormap){
        var n = colormap.length;
        var level =  (value-minValue) / (maxValue - minValue);
        return colormap[Math.floor( level * (n-1)) ];
    }
};



var NewMapComponentDev = UnmanagedComponent.extend(ShapeConversionMixin).extend(ColorMapMixin).extend({
    ph: undefined,
    mapEngine: undefined,
    values: undefined,
    // Properies defined in CDE
    //shapeDefinition: undefined,
    //mapMode: ['', 'markers', 'shapes'][0],
    //shapeSource: '',
    //tilesets: ['mapquest'],
    // colormap: [[0, 102, 0, 255], [255, 255 ,0,255], [255, 0,0, 255]], //RGBA
    tileServices: {
        // list of tileset services that were tested and are working at 2013-11-04, see http://mc.bbbike.org/mc/?num=2 for comparison
        'default': "http://otile{switch:1,2,3,4}.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png", //MapQuest tile server
        'apple': "http://gsp2.apple.com/tile?api=1&style=slideshow&layers=default&lang=en_US&z=${z}&x=${x}&y=${y}&v=9",
        'mapquest': "http://otile{switch:1,2,3,4}.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png", //MapQuest tile server
        'mapquest-normal': "http://otile{switch:1,2,3,4}.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png", //MapQuest tile server
        'mapquest-hybrid': "http://otile{switch:1,2,3,4}.mqcdn.com/tiles/1.0.0/hyb/${z}/${x}/${y}.png", //MapQuest tile server
        'mapquest-sat': "http://otile{switch:1,2,3,4}.mqcdn.com/tiles/1.0.0/sat/${z}/${x}/${y}.jpg", //MapQuest tile server
        'mapbox-world-light': "https://{switch:a,b,c,d}.tiles.mapbox.com/v3/mapbox.world-light/${z}/${x}/${y}.jpg",
        'mapbox-world-dark': "https://{switch:a,b,c,d}.tiles.mapbox.com/v3/mapbox.world-dark/${z}/${x}/${y}.jpg",
        'mapbox-terrain': 'https://{switch:a,b,c,d}.tiles.mapbox.com/v3/examples.map-9ijuk24y/${z}/${x}/${y}.jpg',
        'mapbox-satellite': 'https://{switch:a,b,c,d}.tiles.mapbox.com/v3/examples.map-qfyrx5r8/${z}/${x}/${y}.png',
        'openstreetmaps':  "http://{switch:a,b,c}.tile.openstreetmap.org/${z}/${x}/${y}.png", //OSM tile server
        'openmapsurfer': 'http://129.206.74.245:8001/tms_r.ashx?x=${x}&y=${y}&z=${z}',
        'openmapsurfer-roads': 'http://129.206.74.245:8001/tms_r.ashx?x=${x}&y=${y}&z=${z}',
        'openmapsurfer-semitransparent': 'http://129.206.74.245:8003/tms_h.ashx?x=${x}&y=${y}&z=${z}',
        'openmapsurfer-hillshade': 'http://129.206.74.245:8004/tms_hs.ashx?x=${x}&y=${y}&z=${z}',
        'openmapsurfer-contour': 'http://129.206.74.245:8006/tms_b.ashx?x=${x}&y=${y}&z=${z}',
        'openmapsurfer-administrative': 'http://129.206.74.245:8007/tms_b.ashx?x=${x}&y=${y}&z=${z}',
        'openmapsurfer-roads-grayscale': 'http://129.206.74.245:8008/tms_rg.ashx?x=${x}&y=${y}&z=${z}',
        'map.eu': "http://alpha.map1.eu/tiles/${z}/${y}/${x}.jpg",
        //'naturalearth': "http://www.staremapy.cz/naturalearth/${z}/${x}/${y}.png",
        'stamen': "http://{switch:a,b,c,d}.tile.stamen.com/terrain/${z}/${x}/${y}.jpg",
        'stamen-terrain': "http://{switch:a,b,c,d}.tile.stamen.com/terrain/${z}/${x}/${y}.jpg",
        'stamen-terrain-background': "http://{switch:a,b,c,d}.tile.stamen.com/terrain-background/${z}/${x}/${y}.jpg",
        'stamen-terrain-labels': "http://{switch:a,b,c,d}.tile.stamen.com/terrain-labels/${z}/${x}/${y}.jpg",
        'stamen-toner': "http://{switch:a,b,c,d}.tile.stamen.com/toner/${z}/${x}/${y}.png",
        'stamen-toner-lite': "http://{switch:a,b,c,d}.tile.stamen.com/toner-lite/${z}/${x}/${y}.png",
        'stamen-toner-background': "http://{switch:a,b,c,d}.tile.stamen.com/toner-background/${z}/${x}/${y}.png",
        'stamen-toner-hybrid': "http://{switch:a,b,c,d}.tile.stamen.com/toner-hybrid/${z}/${x}/${y}.png",
        'stamen-toner-labels': "http://{switch:a,b,c,d}.tile.stamen.com/toner-labels/${z}/${x}/${y}.png",
        'stamen-toner-lines': "http://{switch:a,b,c,d}.tile.stamen.com/toner-lines/${z}/${x}/${y}.png",
        'stamen-toner-2010': "http://{switch:a,b,c,d}.tile.stamen.com/toner-2010/${z}/${x}/${y}.png",
        'stamen-toner-2011': "http://{switch:a,b,c,d}.tile.stamen.com/toner-2011/${z}/${x}/${y}.png",
        'stamen-watercolor': "http://{switch:a,b,c,d}.tile.stamen.com/watercolor/${z}/${x}/${y}.jpg",
        'nokia-normal': 'http://maptile.maps.svc.ovi.com/maptiler/maptile/newest/normal.day/${z}/${x}/${y}/256/png8',
        'nokia-normal-grey': 'http://maptile.maps.svc.ovi.com/maptiler/maptile/newest/normal.day.grey/${z}/${x}/${y}/256/png8',
        'nokia-normal-transit': 'http://maptile.maps.svc.ovi.com/maptiler/maptile/newest/normal.day.transit/${z}/${x}/${y}/256/png8',
        'nokia-satellite': 'http://maptile.maps.svc.ovi.com/maptiler/maptile/newest/satellite.day/${z}/${x}/${y}/256/png8',
        'nokia-terrain': 'http://maptile.maps.svc.ovi.com/maptiler/maptile/newest/terrain.day/${z}/${x}/${y}/256/png8',
        'arcgis-street': 'http://services.arcgisonline.com/ArcGIS/rest/services/World_street_Map/MapServer/tile/${z}/${x}/${y}',
        'arcgis-topographic': 'http://services.arcgisonline.com/ArcGIS/rest/services/World_street_Topo/MapServer/tile/${z}/${x}/${y}',
        'arcgis-natgeo': 'http://services.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/${z}/${x}/${y}',
        'arcgis-world': 'http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/${z}/${x}/${y}',
        'arcgis-lightgray': 'http://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Reference/MapServer/tile/${z}/${x}/${y}',
        'arcgis-delorme': 'http://services.arcgisonline.com/ArcGIS/rest/services/Specialty/DeLorme_World_Base_Map/MapServer/tile/${z}/${x}/${y}'

    },
    otherTileServices: [
        // These are tilesets using special code
        'google'
    ],
    tileServicesOptions: {
        // WIP: interface for overriding defaults
        'apple': {minZoom: 3, maxZoom:14}
    },
    // shapeMouseOver : function(event){
    //     Dashboards.log('Currently at lat=' + event.latitude + ', lng=' + event.longitude + ': Beat '+ event.data.key + ':' + event.data.value + ' crimes');
    //     return {
    //         fillColor: 'blue',
    //         fillOpacity: 1,
    //         strokeColor: 'white',
    //         strokeWidth: 2
    //     };

    // },
    // shapeMouseOut: undefined,
    // // function(event){
    // //         return event.style;
    // // },
    // shapeMouseClick: function(event){
    //     return {
    //         fillColor: 'red',
    //         fillOpacity: 1
    //     };
    // },
    // // End

    update : function() {
        this.registerEvents();
        if (_.isString(this.tilesets)){
            this.tilesets = [this.tilesets];
        }

       	if (this.testData) {
            this.render(this.testData);
            return;
       	}
        var myself=this;
        if ( this.queryDefinition && !$.isEmptyObject(this.queryDefinition) ){
            myself.triggerQuery(myself.queryDefinition, function(values) {
                if ( (myself.mapMode == "shapes") && (myself.shapeSource != undefined) && (!_.isEmpty(myself.shapeSource)) )  {
                    /*
                     Ensure the shapes are loaded before rendering, i.e., before attempting to draw the shapes.
                     In this approach, the shapes are loaded after postFetch
                     This could probably be changed to occurr at the same time as triggerQuery.
                     */
                    myself.getShapeDefinition( function () {
                        myself.render(values);
                    });
                } else {
                    // no shapes, let's render
                    myself.render(values);
                }
            });
        } else {
            // No datasource, we'll just display the map
            this.synchronous(this.render, {});
        }

    },

    registerEvents: function(){
        var myself = this;

        this.on('shape:mouseover', function(event){
            if (myself.shapeMouseOver && (typeof myself.shapeMouseOver == 'function')){
                var result = myself.shapeMouseOver(event);
                if (result){
                    event.draw( _.defaults(result, {'zIndex': 1}) );
                }
            }
        });
        this.on('shape:mouseout', function(event){
            if (myself.shapeMouseOut && (typeof myself.shapeMouseOut == 'function')){
                var result = myself.shapeMouseOut(event);
                if (result){
                    event.draw( result );
                } else {
                    event.draw( event.style );
                }
            } else if (myself.shapeMouseOver){
                event.draw( event.style ); // restore original style
            }
        });
        this.on('shape:click', function(event){
            if (myself.shapeMouseClick && (typeof myself.shapeMouseClick == 'function')){
                var result = myself.shapeMouseClick(event);
                if (result){
                    event.draw( result );
                }
            }
        });
    },

    getShapeThenRender: function (values) {
        /*
         Ensure the shapes are loaded before rendering, i.e., before attempting to draw the shapes.
         In this approach, the shapes are loaded after postFetch
         This could probably be changed to occurr at the same time as triggerQuery.
         */
        var myself = this;
        this.getShapeDefinition( function () {
            myself.render(values);
        });
    },

    render: function(values) {
        if (this.shapeDefinition){
            Dashboards.log('Loaded ' + _.keys(this.shapeDefinition).length + ' shapes', 'debug');
        }

        if (this.mapEngineType == 'google') {
            //this.mapEngine = _.extend(new GoogleMapEngine(), Backbone.Events);
            this.mapEngine = new GoogleMapEngine();
        } else {
            this.mapEngine = new OpenLayersEngine();
        }

        this.values = values;
        this.mapEngine.tileServices = this.tileServices;
        this.mapEngine.tileServicesOptions = this.tileServicesOptions;
        this.mapEngine.init(this, this.tilesets);
    },

    initCallBack: function() {

        this.ph = $("#" + this.htmlObject);
        this.ph.empty();
        this.mapEngine.renderMap(this.ph[0], this.centerLongitude, this.centerLatitude, this.defaultZoomLevel);

        switch (this.mapMode){
        case 'shapes':
            this.setupShapes(this.values);
            break;
        case 'markers':
            this.setupMarkers(this.values);
            break;
        }
    },

    getShapeDefinition : function(callback){
        var myself = this;
        if (this.shapeSource && !_.isEmpty(this.shapeSource) && !this.shapeDefinition){
            var filetype = this.shapeSource.split('.').reverse()[0].toLowerCase(); // get extension
	    $.ajax(this.shapeSource, {
                async: true,
                dataType: filetype == 'json' ? 'json' : 'text',
                success: function(data) {
	            if (data)  {
                        myself.rawData = data;
                        //console.log(data.toString().slice(0, 10));

                        switch (filetype){
                        case 'kml':
	                    myself.shapeDefinition = myself.getShapeFromKML(data, myself.parseShapeKey);
                            break;
                        case 'json':
                            myself.shapeDefinition = data;
                            break;
                        }

                        if (typeof myself.postProcessShape == "function") {
                            myself.shapeDefinition = myself.postProcessShape(myself.shapeDefinition);
                        }
                    }
                    callback.apply(myself);
	        }
            });
        }
        // else {
        //     callback.apply(myself);
        //}
    },

    postProcessShape : function(shapeDefinition){
        /*
         Use this e.g. to reduce the number of points in the shapes:

         // var myself = this,
         //      precision_m = 100;
         // _.each(shapeDefinition, function (points, key) {
         //     shapeDefinition[key] =  _.map(points, myself.reducePoints, precision_m);
         // });
         */
        return shapeDefinition;
    },

    setupShapes: function (values) {
        if (!this.shapeDefinition) {
            return;
        }
        if (!values || !values.resultset){
            return;
        }
        //Build an hashmap from metadata
        var mapping = this.getMapping(values);
        var myself = this;

        //Discover which rows correspond to the key and to the value
        var idxKey = 0,
            idxValue = 1;

        // Define default shape appearance
        var shapeSettings = _.defaults(this.shapeSettings, {
	    fillColor:  'blue',
	    fillOpacity: 0.5,
            strokeWidth: 0.5,
            strokeColor: 'black',
            zIndex: 0
	});

        // Attribute a color each shape
        var colormap = this.getColorMap();
        var qvalues = _.map(values.resultset, function (row) { return row[idxValue]; });
        var minValue = _.min(qvalues), maxValue = _.max(qvalues);

        $(values.resultset).each( function (i, elt) {
            var fillColor = myself.mapColor(elt[idxValue], minValue, maxValue, colormap);
            myself.renderShape( myself.shapeDefinition[elt[idxKey]], _.defaults({
                fillColor: fillColor
            }, shapeSettings),{
                rawData: elt,
                key : elt[idxKey],
                value: elt[idxValue],
                minValue: minValue,
                maxValue: maxValue
            });
        });
        this.mapEngine.postSetShapes(this);
    },

    renderShape:  function(shapeDefinition, shapeSettings, data) {
        this.mapEngine.setShape(shapeDefinition, shapeSettings, data, this);
    },

    setupMarkers: function (values)  {
        if (!values || !values.resultset)
            return;

        //Build an hashmap from metadata
        var mapping = this.getMapping(values);
        var myself = this;
        $(values.resultset).each(function (i, elt) {
            var location;
            if (mapping.addressType != 'coordinates') {
                location = myself.getAddressLocation((mapping.address != undefined ? elt[mapping.address]:undefined), mapping.addressType, elt, mapping, i);
            } else {
                location = [elt[mapping.longitude], elt[mapping.latitude]];
                myself.renderMarker(location, elt, mapping, i);
            }
        });

    },

    renderMarker: function(location, elt, mapping, position) {
        var myself = this;
        if (location === undefined) {
            Dashboards.log('Unable to get location for address ' + elt[mapping.address] + '. Ignoring element.', 'debug');
            return true;
        }


        var marker;
        var description;

	var markerWidth = myself.markerWidth;
        if (mapping.markerWidth)
            markerWidth = elt[mapping.markerWidth];
        var markerHeight = myself.markerHeight;
        if (mapping.markerHeight)
            markerHeight = elt[mapping.markerHeight];

        var defaultMarkers = false;

        if (mapping.marker) marker = elt[mapping.marker];
        if (marker == undefined) {
            var st = {data: elt, position: position};
	    var addinName = this.markerImageGetter;

	    //Check for cgg graph marker
	    if (this.markerCggGraph) {
		st.cggGraphName = this.markerCggGraph;
		st.width = markerWidth;
		st.height = markerHeight;
		st.parameters = {};
		$(this.cggGraphParameters).each (function (i, parameter) {
		    st.parameters[parameter[0]] =  elt[mapping[parameter[1]]];
		});
		addinName = 'cggMarker';
	    } else {
		//Else resolve to urlMarker addin
		st.url = myself.marker;
		defaultMarkers = myself.marker == undefined;
		addinName = 'urlMarker';
	    }

            if (!addinName) addinName = 'urlMarker';
	    var addIn = this.getAddIn("MarkerImage",addinName);
	    marker = addIn.call(this.ph, st, this.getAddInOptions("MarkerImage", addIn.name));
        }

        if (mapping.description) description = elt[mapping.description];

        var clickFunction = function(data, mapElement) {

            $(myself.popupParameters).each(function (i, eltA) {
                Dashboards.fireChange(eltA[1], data[ mapping[ eltA[0].toLowerCase() ] ]);
            });

            /*if (myself.markerClickFunction)
             myself.markerClickFunction(data);*/

            if (myself.popupContentsDiv || mapping.popupContents) {
                var contents;
                if (mapping.popupContents) contents = elt[mapping.popupContents];
                var height = mapping.popupContentsHeight ? elt[mapping.popupContentsHeight] : undefined;
                if (!height) height = myself.popupHeight;
                var width = mapping.popupContentsWidth? elt[mapping.popupContentsWidth] : undefined;
                if (!width) width = myself.popupWidth;
                //                    if (!contents) contents = $("#" + myself.popupContentsDiv).html();

		var borderColor = undefined;
		if (defaultMarkers) {
		    switch (position % 5) {
		    case 0:
			borderColor = "#394246";
			break;
		    case 1:
			borderColor = "#11b4eb";
			break;
		    case 2:
			borderColor = "#7a879a";
			break;
		    case 3:
			borderColor = "#e35c15";
			break;
		    case 4:
			borderColor = "#674f73";
			break;
		    }
		}
                myself.mapEngine.showPopup(data,  mapElement, height, width, contents, myself.popupContentsDiv, borderColor);
            }
        };


        Dashboards.log('About to render ' + location[0] + ' / ' + location[1] + ' with marker ' + marker + ' sized ' + markerHeight + ' / ' + markerWidth + 'and description ' + description, 'debug');
        myself.mapEngine.setMarker(location[0], location[1], marker, description, elt, clickFunction, markerWidth, markerHeight);

    },

    getAddressLocation: function (address, addressType, data, mapping, position) {

        var addinName = this.locationResolver;
        if (!addinName) addinName = 'geonames';
        var addIn = this.getAddIn("LocationResolver",addinName);


        target = this.ph;
        var state = {address: address, addressType: addressType, position: position};
        if (mapping.country != undefined) state.country = data[mapping.country];
        if (mapping.city != undefined) state.city = data[mapping.city];
        if (mapping.county != undefined) state.county = data[mapping.county];
        if (mapping.region != undefined) state.region = data[mapping.region];
        if (mapping.state != undefined) state.state = data[mapping.state];
        var myself = this;
        state.continuationFunction = function (location) {
            myself.renderMarker(location, data, mapping, position);
        }
        addIn.call(target,state,this.getAddInOptions("LocationResolver",addIn.name));
    },

    getMapping: function(values) {
        var map = {};

        if(!values.metadata || values.metadata.length == 0)
            return map;

	//Iterate through the metadata. We are looking for the following columns:
	// * address or one or more of 'Country', 'State', 'Region', 'County', 'City'
	// * latitude and longitude - if found, we no longer care for address
	// * description - Description to show on mouseover
	// * marker - Marker image to use - usually this will be an url
	// * markerWidth - Width of the marker
	// * markerHeight - Height of the marker
	// * popupContents - Contents to show on popup window
	// * popupWidth - Width of the popup window
	// * popupHeight - Height of the popup window

	$(values.metadata).each(function (i, elt) {

	    switch (elt.colName.toLowerCase()) {
	    case 'latitude':
		map.addressType = 'coordinates';
		map.latitude = i;
		break;
	    case 'longitude':
		map.addressType = 'coordinates';
		map.longitude = i;
		break;
	    case 'description':
		map.description = i;
		break;
	    case 'marker':
		map.marker = i;
		break;
	    case 'markerwidth':
		map.markerWidth = i;
		break;
	    case 'markerheight':
		map.markerHeight = i;
		break;
	    case 'popupcontents':
		map.popupContents = i;
		break;
	    case 'popupwidth':
		map.popupWidth = i;
		break;
	    case 'popupheight':
		map.popupHeight = i;
		break;
	    case 'address':
		if (!map.addressType) {
		    map.address = i;
		    map.addressType = 'address';
		}
		break;
	    default:
		map[elt.colName.toLowerCase()] = i;
		break;
		// if ($.inArray(values.metadata[0].colName, ['Country', 'State', 'Region', 'County', 'City'])) {
	    }

	});

        return map;
    }


});


var MapEngine = Base.extend({
    tileServices: undefined,
    tileServicesOptions: undefined,
    newLayer: function(name){},
    renderMap: function(){},
    setMarker: function(){},
    showPopup: function(){},
    setShape: function(polygonArray, shapeStyle, data, component){},
    postSetShapes: function() {},
    toNativeStyle: function( foreignStyle){
        var validStyle = {};
        _.each(foreignStyle, function (value, key){
            switch(key){
            case 'visible':
            case 'zIndex':
            case 'fillColor':
            case 'fillOpacity':
            case 'strokeColor':
            case 'strokeOpacity':
            case 'strokeWidth':
            }
        });
        return validStyle;
    },
    wrapEvent: function(event){},
    _selectUrl: function(paramString, urls) {
        /**
         * Method: selectUrl
         * selectUrl() implements the standard floating-point multiplicative
         *     hash function described by Knuth, and hashes the contents of the
         *     given param string into a float between 0 and 1. This float is then
         *     scaled to the size of the provided urls array, and used to select
         *     a URL.
         *
         * Parameters:
         * paramString - {String}
         * urls - {Array(String)}
         *
         * Returns:
         * {String} An entry from the urls array, deterministically selected based
         *          on the paramString.
         */
        var product = 1;
        /**
         * Constant: URL_HASH_FACTOR
         * {Float} Used to hash URL param strings for multi-WMS server selection.
         *         Set to the Golden Ratio per Knuth's recommendation.
         */
        var URL_HASH_FACTOR = (Math.sqrt(5) - 1) / 2;
        for (var i=0, len=paramString.length; i<len; i++) {

            product *= paramString.charCodeAt(i) * URL_HASH_FACTOR;
            product -= Math.floor(product);
        }
        return urls[Math.floor(product * urls.length)];
    },
    _switchUrl: function (url) {
        /*
         * support multiple servers in URL config
         * http://{switch:a,b}.tile.bbbike.org -> ["http://a.tile.bbbike.org", "http://a.tile.bbbike.org" ]
         */
        var list = url.match(/(http[s]?:\/\/[0-9a-z.]*?)\{switch:([a-z0-9,]+)\}(.*)/); // /(http:\/\/[0-9a-z]*?)\{switch:([a-z0-9,]+)\}(.*)/);

        if (!list || list.length == 0) {
            return url;
        }
        var servers = list[2].split(",");
        var url_list = [];
        for (var i = 0; i < servers.length; i++) {
            url_list.push(list[1] + servers[i] + list[3]);
        }
        return url_list;
    },
    _getTileServiceURL: function(name){
        var urlTemplate = this.tileServices[name]; // || this.tileServices['default'],
        if (!urlTemplate){
            // Allow the specification of an url from CDE
            if (name.length>0 && name.contains('{')){
                urlTemplate = name;
                //name = 'custom';
            }
        }
        return  urlTemplate;
    }

});



var GoogleMapEngine = MapEngine.extend({
    map: undefined,
    centered: false,
    overlays: [],
    init: function(mapComponent, tilesets) {
        // API_KEY
        this.tilesets = tilesets;

        $.when( loadGoogleMaps('3',  false) ).then (
            function (status) {
                OurMapOverlay.prototype = new google.maps.OverlayView();
                OurMapOverlay.prototype.onAdd = function() {
                    // Note: an overlay's receipt of onAdd() indicates that
                    // the map's panes are now available for attaching
                    // the overlay to the map via the DOM.

                    // Create the DIV and set some basic attributes.
                    var div = document.createElement('DIV');
                    div.id = 'MapOverlay';
                    div.style.position = "absolute";

                    if (this.borderColor_) {
            	        div.style.border = '3px solid ' + this.borderColor_;
                    } else {
			div.style.border = "none";
                    }


                    /*			var myself = this;
		     var closeDiv = $("<div id=\"MapOverlay_close\" class=\"olPopupCloseBox\" style=\"position: absolute;\"></div>");
		     closeDiv.click(function () {
		     myself.setMap(null);
		     });

		     $(div).append(closeDiv);
                     */
		    if (this.popupContentDiv_ && this.popupContentDiv_.length > 0) {
			$(div).append($('#' + this.popupContentDiv_));
		    } else
	                div.innerHTML = this.htmlContent_;


                    // Set the overlay's div_ property to this DIV
                    this.div_ = div;

                    // We add an overlay to a map via one of the map's panes.
                    // We'll add this overlay to the overlayImage pane.
                    var panes = this.getPanes();
                    panes.overlayLayer.appendChild(div);
                };

                OurMapOverlay.prototype.draw = function() {
                    // Size and position the overlay. We use a southwest and northeast
                    // position of the overlay to peg it to the correct position and size.
                    // We need to retrieve the projection from this overlay to do this.
                    var overlayProjection = this.getProjection();

                    // Retrieve the southwest and northeast coordinates of this overlay
                    // in latlngs and convert them to pixels coordinates.
                    // We'll use these coordinates to resize the DIV.
                    var sp = overlayProjection.fromLatLngToDivPixel(this.startPoint_);

                    // Resize the DIV to fit the indicated dimensions.
                    var div = this.div_;
                    div.style.left = sp.x + 'px';
                    div.style.top = (sp.y + 30) + 'px';
                    div.style.width = this.width_ + 'px';
                    div.style.height = this.height_ + 'px';
                };


                OurMapOverlay.prototype.onRemove = function() {
        	    if (this.popupContentDiv_) {
                        //        	 	$('#' + this.popupContentDiv_).append($(this.div_));
                        //        	 	$(this.div_).detach();
        	    }
        	    this.div_.style.display = 'none';
                    //            this.div_.parentNode.removeChild(this.div_);
                    //           this.div_ = null;
                };

                // var contains = function(s, v){
                //     // check if a string fragment v exists inside any string of an array of strings
                //     return _.some(s, function(t){
                //         return t.split('-')[0] == v;
                //     });
                // };

                // if ( contains(tilesets, 'stamen')){
                //     //$.getScript('http://maps.stamen.com/js/tile.stamen.js?v1.2.3');
                //     loadStamenMaps();
                // }
                mapComponent.initCallBack();

            });
    },

    toNativeStyle: function (foreignStyle){
        var validStyle = {};
        _.each(foreignStyle, function (value, key){
            switch(key){
            case 'strokeWidth':
                validStyle['strokeWeight'] = value;
                break;
            case 'zIndex':
            case 'visible':
            case 'fillColor':
            case 'fillOpacity':
            case 'strokeColor':
            case 'strokeOpacity':
                validStyle[key] = value;
            }
        });
        return validStyle;
    },

    wrapEvent: function (event, data, shapeStyle, shape){
        var myself = this;
        return {
            latitude: event.latLng.lat(),
            longitude: event.latLng.lng(),
            data: data,
            style: _.clone(shapeStyle),
            shape: shape,
            mapEngineType: 'Google',
            draw: function( style ){
                var validStyle = myself.toNativeStyle(style);
                shape.setOptions(validStyle);
                shape.setVisible(false);
                shape.setVisible(_.has(style, 'visible') ? !!style.visible : true);
            },
            raw: event
        };
    },
    setShape: function(polygonArray, shapeStyle, data, component) {
        var myself = this;

        _.each(polygonArray, function(polygon) {
	    var polyPath =  _.map(polygon, function (p) {
                return _.map(p, function (latlong){
	            return new google.maps.LatLng( latlong[0], latlong[1] );
	        });
            });

            var shape = new google.maps.Polygon(_.extend({
	        paths : polyPath
	    }, myself.toNativeStyle(shapeStyle)));
            shape.setMap(myself.map);

            google.maps.event.addListener(shape, 'click', function (event) {
                component.trigger('shape:click', myself.wrapEvent(event, data, shapeStyle, shape));
            });
	    google.maps.event.addListener(shape, 'mousemove',function (event) {
                component.trigger('shape:mouseover', myself.wrapEvent(event, data, shapeStyle, shape));
            });
            google.maps.event.addListener(shape, 'mouseout', function (event) {
                component.trigger('shape:mouseout', myself.wrapEvent(event, data, shapeStyle, shape));
            });
        });

    },

    postSetShapes: function (){},

    setMarker: function(lon, lat, icon, description, data, clickFunction, markerWidth, markerHeight) {
        var myLatLng = new google.maps.LatLng(lat,lon);

        var image = new google.maps.MarkerImage(icon,
                                                // This marker is 20 pixels wide by 32 pixels tall.
                                                new google.maps.Size(markerWidth, markerHeight),
                                                // The origin for this image is 0,0.
                                                new google.maps.Point(0,0),
                                                // The anchor for this image is the base of the flagpole at 0,32.
                                                new google.maps.Point(0, 0));



        var marker = new google.maps.Marker({
            position: myLatLng,
            map: this.map,
            icon: image,
            title: description
        });

        if (clickFunction !== undefined) {
            google.maps.event.addListener(marker, 'click', function () {clickFunction(data, marker);});
        }

        if (!this.centered)
	    this.map.setCenter(myLatLng);
    },

    renderMap: function(target, centerLongitude, centerLatitude, zoomLevel) {
        var myself = this;
        var latlng;

        if (centerLatitude && centerLatitude != '' && centerLongitude && centerLongitude != '') {
	    latlng = new google.maps.LatLng(centerLatitude, centerLongitude);
	    this.centered = true;
	} else
	    latlng = new google.maps.LatLng(38.471, -9.15);

	if (!zoomLevel) zoomLevel = 2;


        var myOptions = {
            zoom: zoomLevel,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        //Prepare tilesets as overlays
        var layers = [],
            layerIds = [],
            layerOptions = [];
        for (var k=0; k<this.tilesets.length; k++) {
            var thisTileset = this.tilesets[k].slice(0),
                tileset = thisTileset.slice(0).split('-')[0],
                variant = thisTileset.slice(0).split('-').slice(1).join('-') || 'default';

            layerIds.push(thisTileset);
            layerOptions.push(_.extend(myOptions, {
                    mapTypeId: thisTileset
            }));

            if (this.tileServices[thisTileset]){
                layers.push(this.newLayer(thisTileset));
            } else {
                layers.push('');
            }

        } //for tilesets

        // Add base map
        this.map = new google.maps.Map(target, {
            mapTypeControlOptions: {
                mapTypeIds: layerIds.concat(_.values(google.maps.MapTypeId))
            }
        });
        for (k=0; k<layers.length; k++){
            if (! _.isEmpty(layers[k])){
                this.map.mapTypes.set(layerIds[k], layers[k]);
                //this.map.overlayMapTypes.push(layers[k]);
                this.map.setMapTypeId(layerIds[k]);
                this.map.setOptions(layerOptions[k]);
            }
        }
    },

    newLayer: function(name){
        var options = _.extend({
                tileSize: new google.maps.Size(256, 256),
                minZoom: 1,
                maxZoom: 19
            }, this.tileServicesOptions[name] || {});
        var urlList = this._switchUrl(this._getTileServiceURL(name));
        var myself = this;

        return new google.maps.ImageMapType(_.defaults({
            name: name.contains('/') ? 'custom' : name,
            getTileUrl: function(coord, zoom) {
                var limit = Math.pow(2, zoom);
                if (coord.y < 0 || coord.y >= limit) {
                    return '404.png';
                } else {
                    // use myself._selectUrl
                    coord.x = ((coord.x % limit) + limit) % limit;
                    var url;
                    if (_.isArray(urlList)){
                        url = urlList[ (coord.x + coord.y + zoom) % urlList.length ];
                        var s =  _.template('${z}/${x}/${y}', {x:coord.x, y:coord.y, z:zoom}, {interpolate: /\$\{(.+?)\}/g});
                        url = myself._selectUrl(s, urlList);
                    } else {
                        url = urlList;
                    }
                    return _.template(url, {x:coord.x, y:coord.y, z:zoom}, {interpolate: /\$\{(.+?)\}/g});
                }
            }
        }, options));
    },

    showPopup: function(data, mapElement, popupHeight, popupWidth, contents, popupContentDiv, borderColor) {
        var overlay = new OurMapOverlay(mapElement.getPosition(), popupWidth, popupHeight, contents, popupContentDiv, this.map, borderColor);

        $(this.overlays).each(function (i, elt) {elt.setMap(null);});
        this.overlays.push(overlay);
    }

});


function OurMapOverlay(startPoint, width, height, htmlContent, popupContentDiv, map, borderColor) {

    // Now initialize all properties.
    this.startPoint_ = startPoint;
    this.width_ = width;
    this.height_ = height;
    this.map_ = map;
    this.htmlContent_ = htmlContent;
    this.popupContentDiv_ = popupContentDiv;
    this.borderColor_ = borderColor;

    this.div_ = null;

    // Explicitly call setMap() on this overlay
    this.setMap(map);
}



var OpenLayersEngine = MapEngine.extend({
    map: undefined,
    markers: undefined,
    centered: false,
    //    featureLayer: undefined,
    useMercator: true,
    init: function(mapComponent, tilesets) {
        var API_KEY = false;
        this.tilesets = tilesets;
        this.mapComponent = mapComponent;
        Dashboards.log ('Requested tilesets:' + JSON.stringify(tilesets), 'debug');

        var contains = function(v){
            return _.some(tilesets, function(tileset){
                //Dashboards.log(tileset, 'debug');
                return tileset.contains(v);
            });
        };

        if (contains('google')) {
            $.when( loadGoogleMaps('3', API_KEY) ).then ( function(){
                mapComponent.initCallBack();
            });
        } else {
            mapComponent.initCallBack();
        }
    },

    showPopup: function(data,  mapElement, popupHeight, popupWidth, contents, popupContentDiv, borderColor) {

        var feature = mapElement;

        if (popupContentDiv && popupContentDiv.length > 0) {
            var div = $('<div>');
            div.append($('#' + popupContentDiv));
            contents = div.html();
        }

        var name = "featurePopup";
        if (borderColor != undefined) {
            name = name + borderColor.substring(1);
        }

        popup = new OpenLayers.Popup.Anchored(name,
                                              feature.lonlat,
                                              new OpenLayers.Size(popupHeight,popupWidth),
                                              contents,
                                              null, true, null);

        feature.popup = popup;
        popup.feature = feature;

        $(this.map.popups).each(function (i, elt) {elt.hide();});

        this.map.addPopup(popup);
    },

    setShape: function(polygonArray, shapeStyle, data, component) {
        var myself =this;
        var proj = new OpenLayers.Projection("EPSG:4326"),  // transform from WGS 1984 //4326
            mapProj = this.map.getProjectionObject();
        _.each(polygonArray, function(polygon){
	    var polyArray =  _.map(polygon, function (p) {
                var polyPath = _.map(p, function (latlong){
                    var point = new OpenLayers.LonLat(latlong[1], latlong[0] ).transform(
                        proj, // transform from WGS 1984
                        mapProj // to the map system
                    );
                    return new OpenLayers.Geometry.Point(point.lon, point.lat);
	        });
                return new OpenLayers.Geometry.LinearRing(polyPath);
            });

            var shape = new OpenLayers.Geometry.Polygon(polyArray);
            var feature = new OpenLayers.Feature.Vector(shape, {data: data, style: shapeStyle}, myself.toNativeStyle(shapeStyle) );
            myself.shapes.addFeatures([feature]);
        });

    },

    postSetShapes: function(){},

    toNativeStyle: function (foreignStyle){
        var validStyle = {};
        _.each(foreignStyle, function (value, key){
            switch(key){
            case 'visible':
                validStyle['display'] = value ? true : 'none';
                break;
            case 'zIndex':
                validStyle['graphicZIndex'] = value;
                break;
            case 'fillColor':
            case 'fillOpacity':
            case 'strokeColor':
            case 'strokeOpacity':
            case 'strokeWidth':
                validStyle[key] = value;
            }
        });
        return validStyle;
    },

    wrapEvent: function (event){
        var coords = this.map.getLonLatFromPixel(
            this.map.getControlsByClass("OpenLayers.Control.MousePosition")[0].lastXy)
                .transform(this.map.getProjectionObject(), new OpenLayers.Projection('EPSG:4326')
                          );
        var shape = event.feature.layer.getFeatureById(event.feature.id);
        var myself = this;
        return {
            latitude: coords.lat,
            longitude: coords.lon,
            data: event.feature.attributes.data,
            shape: shape,
            style: event.feature.attributes.style,
            mapEngineType: 'OpenLayers',
            draw: function( style ){
                var validStyle = myself.toNativeStyle(style);
                event.feature.layer.drawFeature(shape, validStyle);
            },
            raw: event

        };
    },

    setShapesCallbacks: function(){
        var myself = this;

        var hoverCtrl = new OpenLayers.Control.SelectFeature(this.shapes, {
            hover: true,
            highlightOnly: true,
            renderIntent: "temporary",
            //clickout: true,
            eventListeners: {
                featurehighlighted: function(e) { myself.mapComponent.trigger('shape:mouseover', myself.wrapEvent(e)); },
                featureunhighlighted:  function(e) { myself.mapComponent.trigger('shape:mouseout', myself.wrapEvent(e)); },
                featureselected: function(e) { myself.mapComponent.trigger('shape:click', myself.wrapEvent(e)); }
            }
        });
        this.map.addControl(hoverCtrl);
        hoverCtrl.activate();

        var clickCtrl = new OpenLayers.Control.SelectFeature(this.shapes, {
            clickout: true
        } );
        this.map.addControl(clickCtrl);
        clickCtrl.activate();

    },

    setMarker: function(lon, lat, icon, description, data, clickFunction, markerWidth, markerHeight) {
	var size = new OpenLayers.Size(markerWidth,markerHeight);
    	var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
	var iconObj = new OpenLayers.Icon(icon,size,offset);
        var marker, feature;


    	if(this.useMercator){
	    marker = new OpenLayers.Marker(lonLatToMercator(new OpenLayers.LonLat(lon,lat)),iconObj);
	    //create a feature to bind marker and record array together
	    feature = new OpenLayers.Feature(this.markers,lonLatToMercator(new OpenLayers.LonLat(lon,lat)),data);
	} else {
	    marker = new OpenLayers.Marker(new OpenLayers.LonLat(lon,lat),iconObj);
	    feature = new OpenLayers.Feature(this.markers,new OpenLayers.LonLat(lon,lat),data);
	}

        feature.marker = marker;

        //create mouse down event for marker, set function to marker_click
        if (clickFunction != undefined){
            // Why mousedown and not click?
	    marker.events.register('mousedown', feature, function (evt) {clickFunction(data, feature);});
        }
        /*
         if (description) {
         var ttips = new OpenLayers.Control.ToolTips({bgColor:"black",textColor:"white", bold : true, opacity : 0.50});
    	 this.map.addControl(ttips);
         var funcOnMouseOver = function (evt) {
         ttips.show({html:description});
         };

         var funcOnMouseOut = function (evt) {
         ttips.hide();
         };

    	 marker.events.register('mouseover', feature, funcOnMouseOver);
	 marker.events.register('mouseout', feature, funcOnMouseOut);
         }
         */
        this.markers.addMarker(marker);

        if (!this.centered)
            this.map.setCenter(marker.lonlat);
    },

    renderMap: function(target, centerLongitude, centerLatitude, zoomLevel) {
        Dashboards.log('Entered renderMap', 'debug');
        var myself = this;
        var useLayerControl = false;
        var customMap = false;
        var centerPoint;

	if (centerLongitude && centerLongitude != '' && centerLatitude && centerLatitude != '') {
	    if(this.useMercator) {
		centerPoint = lonLatToMercator(new OpenLayers.LonLat(centerLongitude,centerLatitude));
	    }else{
		centerPoint = new OpenLayers.LonLat(centerLongitude,centerLatitude);
	    }
	}

    	this.map = new OpenLayers.Map(target, {
            maxExtent: new OpenLayers.Bounds(-20037508,-20037508,20037508,20037508),
            numZoomLevels: 18,
            maxResolution: 156543,
            units: 'm',
            zoomDuration: 10, // approximately match Google's zoom animation
            displayProjection: new OpenLayers.Projection("EPSG:4326"),
            projection: "EPSG:900913"
        });
        //OpenLayers.ImgPath = 'resources/components/NewMapComponent/openlayers_themes/dark/'; //theme the buttons
        var myOptions = {
            type: 'png',
            transparent: 'true',
            transitionEffect: 'resize',
            displayOutsideMaxExtent: true
        };
        var layer;
        for (var k=0; k< this.tilesets.length; k++){
            var thisTileset = this.tilesets[k],
                tileset = this.tilesets[k].slice(0).split('-')[0],
                variant = this.tilesets[k].slice(0).split('-').slice(1).join('-') || 'default';
            Dashboards.log('Tilesets: ' + JSON.stringify(this.tilesets)  + ', handling now :' + thisTileset + ', ie tileset ' + tileset + ', variant ' + variant);
            switch(tileset){
            case 'google':
                layer =  new OpenLayers.Layer.Google("Google Streets", {visibility: true, version: '3'});
                break;

            case 'opengeo':
                layer = new OpenLayers.Layer.WMS( thisTileset,
                    "http://maps.opengeo.org/geowebcache/service/wms",
                    {
	                layers: variant,
	                //format: format,
	                bgcolor: '#A1BDC4'
	            },
	            {
                        wrapDateLine: true,
                        transitionEffect: 'resize'
                    });
                break;

            default:
                layer = this.newLayer(thisTileset);
                break;
            }

    	    // add the OpenStreetMap layer to the map
	    this.map.addLayer(layer);
        }

        // add layers for the markers and for the shapes
        this.markers = new OpenLayers.Layer.Markers( "Markers" );
        this.shapes = new OpenLayers.Layer.Vector( "Shapes" );
	this.map.addLayers([this.markers, this.shapes]);

        this.setShapesCallbacks();

        // add control
        this.map.addControl(new OpenLayers.Control.LayerSwitcher());
        this.map.addControl(new OpenLayers.Control.MousePosition());
        this.map.addControl(new OpenLayers.Control.PanZoomBar());
	this.map.addControl(new OpenLayers.Control.MouseDefaults());
	this.map.addControl(new OpenLayers.Control.KeyboardDefaults());

	//set center and zoomlevel of the map
	if (centerPoint) {
	    this.map.setCenter(centerPoint);
	    this.centered = true;
	} else {
	    this.map.setCenter(lonLatToMercator(new OpenLayers.LonLat(-9.15,38.46)));
	}

	if (zoomLevel != '')
	    this.map.zoomTo(zoomLevel);

        Dashboards.log('Exited renderMap', 'debug');
        this.postRenderMap();

    },

    postRenderMap: function(){
        // var layer = new OpenLayers.Layer.GML("KML", "static/custom/maps/PoliceBeats.kml", {
        //     projection: "EPSG:4326",
        //     format: OpenLayers.Format.KML,
        //     formatOptions: {
        //         'extractStyles': true
        //     }
        // });
        // this.map.addLayer(layer);
    },

    newLayer: function(name){
        var urlTemplate = this.tileServices[name]; // || this.tileServices['default'],
        var options = _.extend({
            "transitionEffect": "resize"
            }, this.tileServicesOptions[name] || {});
        if (!urlTemplate){
            // Allow the specification of an url from CDE
            if (name.length>0 && name.contains('{')){
                urlTemplate = name;
                name = 'custom';
            }
        }
        return new OpenLayers.Layer.XYZ( name, this._switchUrl(urlTemplate), _.defaults({}, options));

    }
});
